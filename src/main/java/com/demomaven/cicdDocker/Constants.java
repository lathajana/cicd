package com.demomaven.cicdDocker;

public interface Constants {
	String CHROME_KEY="webdriver.chrome.driver";
	String path = System.getProperty("user.dir");
	String CHROME_VALUE = path + "/driver/chromedriver";
	
	String PROPERTYFILE_PATH="./data.properties";
	String PHOTO_PATH="./Photo/";
}
