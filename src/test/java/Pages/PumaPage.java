package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import com.demomaven.cicdDocker.GenericPage;

import junit.framework.Assert;

public class PumaPage extends GenericPage{

	public PumaPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	@FindBy(xpath="//ul/li[1]/a[@href='/in/en/women']")
	private WebElement WomenHover;
	
	
	public void women()
	{
		mouseHover(WomenHover);
	}
	
	@FindBy(xpath="//li/div/a[@href='/in/en/women/shoes']")
	 private WebElement wrunning;
	 
	 public void clickWrunning()
	 {
		
		
			 wrunning.click();
			 Assert.assertTrue(true);
		
	 }
	 
	 @FindBy(xpath="//ul/li[2]/a[@href='/in/en/men']")
		private WebElement Men;
		
		
		public void men()
		{
			mouseHover(Men);
		}
		
		@FindBy(xpath="//li/div/a[@href='/in/en/men/shoes']")
		 private WebElement shoes;
		
		 WebDriverWait wait=new WebDriverWait(driver, 10);
		 public void shoes()
		
		 {	wait.until(ExpectedConditions.elementToBeClickable(shoes));
			 shoes.click();
			 Assert.assertTrue(true);
			
		 }
	 
	

}
