package com.demomaven.cicdDocker;

import java.util.concurrent.TimeUnit;

import org.testng.Assert;
import org.testng.annotations.Test;

import Pages.PumaPage;

public class TestPuma extends BaseTests {
	
	@Test
	public void testPuma() {
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		PumaPage p=new PumaPage(driver);
		driver.manage().window().maximize();
		String title=driver.getTitle();
		System.out.println(title);
		Assert.assertEquals("PUMA.COM | Forever Faster", title);
		p.women();
		p.clickWrunning();
}
	@Test
	public void men() {
		driver.manage().timeouts().pageLoadTimeout(50, TimeUnit.SECONDS);
		PumaPage p=new PumaPage(driver);
		driver.manage().window().maximize();
		p.men();
		p.shoes();
		String title=driver.getTitle();
		System.out.println(title);
		Assert.assertEquals("PUMA Men's Shoes - Buy Casual & Sports Shoes for Men - PUMA", title);
		
	}
	
	}

